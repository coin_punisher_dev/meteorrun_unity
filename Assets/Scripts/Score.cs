﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Score : MonoBehaviour
{
    public TextMeshProUGUI endScoreText;

    [HideInInspector]
    public int score = 0;

    private Transform playerTransform;
    private TextMeshProUGUI scoreText;
    private float nextScore = 10f;

    void Start()
    {
        playerTransform = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        scoreText = GetComponent<TextMeshProUGUI>();
    }

    public void IncreaseScore()
    {
        nextScore += 10f;
        score++;
        endScoreText.text = scoreText.text = score.ToString();
    }

    void Update()
    {
        if (playerTransform != null)
        {
            if (playerTransform.position.y >= nextScore)
                IncreaseScore();
        }
    }
}
