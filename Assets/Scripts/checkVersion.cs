﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleFirebaseUnity;
using SimpleFirebaseUnity.MiniJSON;
using SimpleJSON;

public class checkVersion : MonoBehaviour
{
	public string game_name;
	public GameObject hideLogin, ShowVersionUpdate;
    // Start is called before the first frame update
    void Start()
    {	
		StartCoroutine(GameCheckVersion());
	}
	
	IEnumerator GameCheckVersion()
    {
		
		Debug.Log("==== Wait for seconds 3f ======");
        
		SimpleFirebaseUnity.Firebase root_url_firebase = SimpleFirebaseUnity.Firebase.CreateNew("https://bossgame-601e5.firebaseio.com");
		SimpleFirebaseUnity.Firebase game_collections = root_url_firebase.Child("game_version_collections", true);
        // Init callbacks
        game_collections.OnGetSuccess += GetValueGameVersion;
		
		game_collections.GetValue();
		
		yield return new WaitForSeconds(3f);
		Debug.Log("==== Finish for seconds 3f ======");
	}
	
	void GetValueGameVersion(SimpleFirebaseUnity.Firebase sender, DataSnapshot snapshot)
    {
		var gameV_data = JSON.Parse(snapshot.RawJson);
		string adata = gameV_data[game_name].ToString();
		string cleanedUpString = adata.Trim('"');
		Debug.Log(cleanedUpString);
		if(cleanedUpString!=GlobalVar.version_game){
			hideLogin.SetActive(false);
			ShowVersionUpdate.SetActive(true);
		}
    }
	
	public void OpenGameNewVersion(string package_game){
		Application.OpenURL ("market://details?id="+package_game);
	}
	
    // Update is called once per frame
    void Update()
    {
        
    }
}
