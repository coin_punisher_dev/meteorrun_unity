﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System.Linq;
using System.Text;
using UnityEngine.Networking;
using UnityEngine.EventSystems;
using GoogleMobileAds.Api;
using System;

public class Quit : MonoBehaviour {

    void OnMouseDown()
    {
        Login Udata = new Login();
        Debug.Log(PlayerPrefs.GetString("bossgame_nickname"));
        StartCoroutine(Udata.postQuit(PlayerPrefs.GetString("bossgame_nickname")));
        StartCoroutine(Udata.postPlayTimePlayingGame(PlayerPrefs.GetString("bossgame_iduser"), "22", PlayerPrefs.GetString("FirstPlay"), System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
        Debug.Log(PlayerPrefs.GetString("FirstPlay") + " " + System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
        Application.Quit();
    }

    public void GUIQuit()
    {
        Login Udata = new Login();
        Debug.Log(PlayerPrefs.GetString("bossgame_nickname"));
        StartCoroutine(Udata.postQuit(PlayerPrefs.GetString("bossgame_nickname")));
        StartCoroutine(Udata.postPlayTimePlayingGame(PlayerPrefs.GetString("bossgame_iduser"), "22", PlayerPrefs.GetString("FirstPlay"), System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
        Debug.Log(PlayerPrefs.GetString("FirstPlay") + " " + System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
        Application.Quit();
    }
}
