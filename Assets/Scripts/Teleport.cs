﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleport : MonoBehaviour {

    public Transform nextPoint, playerPos;      //you have to add these manually. if the teleport is the last one, then you may not add 'nextPoint' to it
    public float speed;

    private bool thisTeleport = false, hit = false;     //thisTeleport is true if the actual gameobject is teleporting; hit is true if we reached the 'nextPoint' 

    public void Start()
    {
        playerPos = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();       //initializes the transform of the player
    }

    public void Update()
    {
        if ((this.gameObject.tag != "LastTeleport") && (GameObject.FindGameObjectWithTag("Player") != null))      //if this is not the last teleport \ we have 'nextPoint' and the player is not destroyed
        {
            if ((FindObjectOfType<Ball>().teleporting) && (thisTeleport))
            {
                playerPos.position = Vector3.MoveTowards(playerPos.position, nextPoint.position, speed * Time.deltaTime);       //moving the player towards the next teleport's position by the speed value
                if (playerPos.position == nextPoint.position)       //if the player reaches the next teleport
                {
                    thisTeleport = false;
                    FindObjectOfType<Ball>().teleporting = false;       //ball is not teleporting
                }
            }
        }
    }

    public void OnTriggerStay2D(Collider2D collision)
    {
        if ((collision.gameObject.tag == "Player") && (FindObjectOfType<Ball>().teleporting == false))      //if the player is not teleporting
        {
            if (this.tag != "LastTeleport")     //if this is not the last teleport
            {
                //spawns particles and after 1 sec deystroys them
                Destroy(Instantiate(FindObjectOfType<Ball>().ringEffect, transform.position, Quaternion.identity).gameObject, 1f);
                Destroy(Instantiate(FindObjectOfType<Ball>().keyUp, transform.position, Quaternion.identity).gameObject, 1f);

                hit = true;
                FindObjectOfType<Ball>().teleporting = true;
                thisTeleport = true;
                collision.gameObject.GetComponent<Rigidbody2D>().Sleep();       //stops the player
                FindObjectOfType<Trajectory>().enabled = false;       //can't aim; can't slow down time

                FindObjectOfType<AudioManager>().TeleportSound();       //Plays 'TeleportSound'
            }
            else        //if this is the last teleport
            {
                FindObjectOfType<Trajectory>().enabled = true;
            }
        }
    }

}
