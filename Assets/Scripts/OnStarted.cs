﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OnStarted : MonoBehaviour
{
	
	public IEnumerator checkInternetConnection(System.Action<bool> action)
    {
        WWW www = new WWW("http://google.com");
        yield return www;
        if (www.error != null)
        {
            action(false);
        }
        else
        {
            action(true);
        }
    }
	
    // Start is called before the first frame update
    void Start()
    {
		PlayerPrefs.SetString("FirstPlay", System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
        Screen.SetResolution(720, 1280, true);
		StartCoroutine(checkInternetConnection((isConnected) => {
            if (!isConnected) {
                SceneManager.LoadScene(3);
            }
        }));
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
