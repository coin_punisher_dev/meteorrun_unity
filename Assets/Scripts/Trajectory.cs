﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Trajectory : MonoBehaviour
{

    public float dotSize;
    public float distanceBetweenDots;
    public float startDotDistance;      //the first dot's and the ball's distance
    public float shootingPowerX;
    public float shootingPowerY;
    public GameObject[] dots;       //'Trajectory' children

    public float maxShootingDistance = 10f, slowMotion = 0.2f;
    public ParticleSystem keyUp;
    public Animation ballAnim;
    public GameObject pressedObj;

    private GameObject trajectory;
    private GameObject ball;
    private Rigidbody2D ballRb;
    private Vector3 ballPos;
    private Vector3 fingerPos;      //position of the pressed down finger/cursor on the screen 
    private Vector3 pressedHoldDistance;                //distance of the 'pressed' and the 'holded' (second) finger's position
    private Vector2 shootingForce;
    private float x1, y1;

    private Vector3 pressedPos;
    private bool newPressed = true;
    private GameObject newObj;

    void Start()
    {
        ball = gameObject;
        ballRb = GetComponent<Rigidbody2D>();

        trajectory = GameObject.FindGameObjectWithTag("Trajectory");

        trajectory.transform.localScale = new Vector3(dotSize, dotSize, trajectory.transform.localScale.z);        //scales the dots by the given values
        trajectory.SetActive(false);       //hides the 'Trajectory' at start

    }

    void Update()
    {
        if (FindObjectOfType<GameManager>().gameHasStarted)
        {



            if ((ballRb.velocity.x * ballRb.velocity.x) + (ballRb.velocity.y * ballRb.velocity.y) <= 0.0085f)       //if the ball is moving really slow
            {
                ballRb.velocity = new Vector2(0f, 0f);      //then don't move it
            }
            else
            {
                trajectory.SetActive(false);        //don't allow the trajectory to be shown. (This is for if you're in the process of aiming and something causes the ball to move)
            }

            ballPos = ball.transform.position;

            if (Input.GetKey(KeyCode.Mouse0))       //if you touch the screen/click on the screen
            {
                Time.timeScale = slowMotion;        //slowes down time by the given value

                fingerPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);        //the position of your finger/cursor
                fingerPos.z = 0;

                if (newPressed)     //if this is the first touch of the shot
                {
                    newObj = Instantiate(pressedObj, Camera.main.transform);
                    newObj.transform.position = pressedPos = fingerPos;     //a gameobject represents the first touch's position
                    newPressed = false;     //the next touch will be the second touch
                }

                pressedHoldDistance = newObj.transform.position - fingerPos;        //calculates the distance between the first touch's position and the secon touch

                shootingForce = new Vector2(pressedHoldDistance.x * shootingPowerX, pressedHoldDistance.y * shootingPowerY);        //adding force

                //setting boundaries----------
                if (shootingForce.x > maxShootingDistance)
                    shootingForce.x = maxShootingDistance;
                else if (shootingForce.x < -maxShootingDistance)
                    shootingForce.x = -maxShootingDistance;

                if (shootingForce.y > maxShootingDistance)
                    shootingForce.y = maxShootingDistance;
                else if (shootingForce.y < -maxShootingDistance)
                    shootingForce.y = -maxShootingDistance;
                //----------------------------

                if ((Mathf.Sqrt((pressedHoldDistance.x * pressedHoldDistance.x) + (pressedHoldDistance.y * pressedHoldDistance.y)) > (0.4f)))       //if the distance is big enough
                {
                    trajectory.SetActive(true);
                }

                for (int i = 0; i < dots.Length; i++)
                { //setting dots position
                    x1 = ballPos.x + shootingForce.x * Time.fixedDeltaTime * (distanceBetweenDots * i + startDotDistance);      //X position for each point is found
                    y1 = ballPos.y + shootingForce.y * Time.fixedDeltaTime * (distanceBetweenDots * i + startDotDistance) - (-Physics2D.gravity.y / 2f * Time.fixedDeltaTime * Time.fixedDeltaTime * (distanceBetweenDots * i + startDotDistance) * (distanceBetweenDots * i + startDotDistance));    //Y position for each point is found
                    dots[i].transform.position = new Vector3(x1, y1, dots[i].transform.position.z);     //position is applied to each point
                }
            }


            if (Input.GetKeyUp(KeyCode.Mouse0))     //if you end your touch/click
            {
                Time.timeScale = 1f;        //sets back time

                Destroy(newObj);        //destroys the firs touch position
                if ((pressedHoldDistance.x > 0.5f || pressedHoldDistance.x < -0.5f) || (pressedHoldDistance.y > 0.5f || pressedHoldDistance.y < -0.5f))     //if distance is big enough
                {
                    ballAnim.Stop();        //stops the first looping animation of the ball
                    ballRb.velocity = new Vector2(1.5f * shootingForce.x, 1.5f * shootingForce.y);      //adds the force to the ball
                    Destroy(Instantiate(keyUp, transform.position, Quaternion.identity).gameObject, 2f);        //spawns a particle system, then destroys it after 2 secs

                    if (!FindObjectOfType<Ball>().revived)
                        FindObjectOfType<Ball>().Invoke("CanDie", 0.1f);
                    ball.GetComponent<Rigidbody2D>().simulated = true;
                }

                newPressed = true;      //next touch/click will be the first one of the next shot

                if (trajectory.activeInHierarchy)        //if the player was aiming...
                {
                    trajectory.SetActive(false);
                    if (ballRb.isKinematic == true)
                        ballRb.isKinematic = false;
                }
            }
        }
    }
}

