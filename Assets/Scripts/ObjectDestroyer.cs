﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectDestroyer : MonoBehaviour {


    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            if ((FindObjectOfType<Ball>().teleporting == false) && (FindObjectOfType<LevelEnd>().levelDone == false))       //if the player is not teleporting and the player didn' t reach the end of the level
            {
                FindObjectOfType<Ball>().EndGame();     //game over
            }  
        }
    }

    public void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag != "Player")
            Destroy(collision.gameObject);      //if anythig reaches the end of this collider, it gets destroyed because we don't need it anymore
    }
}
