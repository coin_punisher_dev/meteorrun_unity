﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Logo : MonoBehaviour
{
    public Image logo_meteor_fire;
    public GameObject gameobject_logo_meteor_fire;
    public double sec_logo_meteor_fire_alpha = 0;
    public decimal lsXm, lsYm;
    public float lsX, lsY;
    // Start is called before the first frame update
    void Start()
    {
        logo_meteor_fire.color = new Color(1f, 1f, 1f, 0f);
        logo_meteor_fire.color = new Color(1f, 1f, 1f, 0.1f);
    }

    // Update is called once per frame
    void Update()
    {
        lsXm = (decimal)gameobject_logo_meteor_fire.transform.position.x - 45m;
        lsX = (float)lsXm;
        lsYm = (decimal)gameobject_logo_meteor_fire.transform.position.y - 45m;
        lsY = (float)lsYm;

        if (lsX <= 455.1) {
            logo_meteor_fire.color = new Color(1f, 1f, 1f, 1f);
        }
        else
        {
            gameobject_logo_meteor_fire.transform.position = new Vector3(lsX, lsY, gameobject_logo_meteor_fire.transform.position.z);
            sec_logo_meteor_fire_alpha++;
            if (sec_logo_meteor_fire_alpha == 1)
            {
                logo_meteor_fire.color = new Color(1f, 1f, 1f, 0f);
            } else if (sec_logo_meteor_fire_alpha == 2)
            {
                logo_meteor_fire.color = new Color(1f, 1f, 1f, 0.2f);
            }
            else if (sec_logo_meteor_fire_alpha == 3)
            {
                logo_meteor_fire.color = new Color(1f, 1f, 1f, 0.3f);
            }
            else if (sec_logo_meteor_fire_alpha == 4)
            {
                logo_meteor_fire.color = new Color(1f, 1f, 1f, 0.4f);
            }
            else if (sec_logo_meteor_fire_alpha == 5)
            {
                logo_meteor_fire.color = new Color(1f, 1f, 1f, 0.6f);
            }
            else if (sec_logo_meteor_fire_alpha == 6)
            {
                logo_meteor_fire.color = new Color(1f, 1f, 1f, 0.7f);
            }
            else if (sec_logo_meteor_fire_alpha == 7)
            {
                logo_meteor_fire.color = new Color(1f, 1f, 1f, 0.8f);
            }
        }

    }

}
