﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Networking;

public class GameManager : MonoBehaviour {
    public Text Score, HighScoreText;
    public TextMeshProUGUI scoreText;
    public GameObject startPanel, endPanel, levelBar, endLevelBar, ringEffect, muteImage, reviveButton, player;
    public SpriteRenderer playerSkin;
    public bool gameHasStarted = false;

    private bool settingsButtonON = false;

    public IEnumerator PostRunningAds(string _param, string _message, int _ads, double _amount)
    {
        Login Udata = new Login();
        WWWForm form = new WWWForm();

        form.AddField("memberID", _param);
        form.AddField("gameID", "22");
        form.AddField("message", _message);
        form.AddField("ads", _ads);
        form.AddField("amount", _amount.ToString());

        UnityWebRequest www = UnityWebRequest.Post(Udata.base_url.ToString() + "runningAds.php", form);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log(www.downloadHandler.text);
        }

    }

    void Start () {
        Initialization();
    }

    public void Initialization()
    {
        startPanel.SetActive(true);
        if (HighScoreText != null)
            HighScoreText.enabled = false;
        if (scoreText != null)
            scoreText.enabled = false;
            Score.enabled = false;
        levelBar.SetActive(false);
        AudioCheck();
        PlayerPrefs.SetString("time_firstgp_str", System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
        PlayerPrefs.SetString("time_endgp_str", "");
        PlayerPrefs.SetString("TimePlayinggp1", PlayerPrefs.GetString("time_firstgp_str"));
        PlayerPrefs.SetString("TimePlayinggp2", PlayerPrefs.GetString("time_firstgp_str"));

        PlayerPrefs.SetString("statusPlay", "1");
        PlayerPrefs.SetString("statusAds", "0");
    }
	
    public void StartButton()
    {
        gameHasStarted = true;
        startPanel.SetActive(false);
        levelBar.SetActive(true);
        if (scoreText != null)
            scoreText.enabled = true;
            Score.enabled = true;
    }

    public void RestartLevelButton()
    {
        //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        PlayerPrefs.SetString("statusAds", "1");
        SceneManager.LoadScene(2);
    }

    public void ClearedPanelActivation()
    {
        levelBar.gameObject.SetActive(false);

    }

    public void EndPanelActivation()
    {
        levelBar.gameObject.SetActive(false);
        endPanel.SetActive(true);
        endLevelBar.GetComponent<Image>().fillAmount = levelBar.GetComponent<Image>().fillAmount;

        if(HighScoreText != null)
        {
            HighScoreText.enabled = true;

            if (PlayerPrefs.GetInt("HighScore", 0) < FindObjectOfType<Score>().score)
                PlayerPrefs.SetInt("HighScore", FindObjectOfType<Score>().score);

            HighScoreText.text = "BEST RECORD: " + PlayerPrefs.GetInt("HighScore", 0).ToString();
        }

        if (scoreText != null)
        {
            scoreText.enabled = false;
            Score.enabled = false;
        }

    }

    public void SkinsPanelActivation()
    {
        startPanel.SetActive(false);
    }

    public void BackButton()
    {
        startPanel.SetActive(true);
    }

    public void SkinButton(Image skin)
    {
        Destroy(Instantiate(ringEffect, playerSkin.gameObject.transform.position, Quaternion.identity), 2f);
        playerSkin.sprite = skin.sprite;
    }

    public void NextLevelButton()
    {
        if (SceneManager.GetActiveScene().buildIndex + 1 != SceneManager.sceneCountInBuildSettings)     //if this is not the last scene
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);       //then load the next scene
        else
            Debug.Log("No more scenes!");
    }

    public void SettingsButton()
    {
        if (settingsButtonON == false)
        {
            settingsButtonON = true;
        }
        else
        {
            settingsButtonON = false;
        }

    }

    public void AudioCheck()
    {
        if (PlayerPrefs.GetInt("Audio", 0) == 0)
        {
            muteImage.SetActive(false);
            FindObjectOfType<AudioManager>().soundIsOn = true;
            FindObjectOfType<AudioManager>().PlayBackgroundMusic();
        }
        else
        {
            muteImage.SetActive(true);
            FindObjectOfType<AudioManager>().soundIsOn = false;
            FindObjectOfType<AudioManager>().StopBackgroundMusic();
        }
    }

    public void AudioButton()
    {
        FindObjectOfType<AudioManager>().ButtonClickSound();
        if (PlayerPrefs.GetInt("Audio", 0) == 0)
            PlayerPrefs.SetInt("Audio", 1);
        else
            PlayerPrefs.SetInt("Audio", 0);
        AudioCheck();
    }

    public void EndlessModeButton()
    {
        SceneManager.LoadScene(0);
    }

    public void AdventureModeButton()
    {
        SceneManager.LoadScene(1);
    }

    public void Revive()
    {
        if (scoreText != null)
            scoreText.enabled = true;
            Score.enabled = true;
        
        endPanel.SetActive(false);
        reviveButton.SetActive(false);

        FindObjectOfType<Ball>().Revive();
    }
}
