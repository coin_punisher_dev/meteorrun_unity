﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Skin : MonoBehaviour {

    public int thisSkin;        //every gameobject that use this script have to have a unique integer!!! set them in the Inspector if you add more skins

    private Image skin;     //actual skin

    public void Awake()
    {
        SkinCheck();        //checks for actual skin
    }

    public void SkinChange()
    {
        PlayerPrefs.SetInt("Skin", thisSkin);       //the game will remember the index of the actual skin
        skin = transform.GetChild(0).GetComponent<Image>();     //sets the skin variable to the actual skin
        FindObjectOfType<GameManager>().SkinButton(skin);     //player's skin will be set to this skin

        FindObjectOfType<AudioManager>().SkinSwitchSound();     //Plays 'SkinSwitchSound'
    }

    public void SkinCheck()
    {
        if (PlayerPrefs.GetInt("Skin", 0) == thisSkin)      //the game checks if this is the skin we last used, and if this is
        {
            SkinChange();       //then changes the player's default skin to this skin
        }
    }

}
