﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject[] obstacles;

    private bool canSpawn = true;
    private Transform playerTransform;

    void Start()
    {
        playerTransform = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();     //initializes the transform of the player
    }

    void Update()
    {
        if (playerTransform != null)
        {
            if (Vector3.Distance(playerTransform.position, transform.position) <= 13f)
                TryToSpawn();
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Obstacle") || collision.CompareTag("Untagged"))         //if an obstacle collides with the spawner, then it cannot spawn
            canSpawn = false;
    }

    void Spawn()
    {
        Instantiate(obstacles[Random.Range(0, obstacles.Length)], transform.position, Quaternion.identity);     //spawns a random obstacle
    }

    void TryToSpawn()
    {
        if (canSpawn)
            Spawn();
        else
        {
            transform.position = new Vector3(0f, transform.position.y + 10f, 0f);
            canSpawn = true;
            Invoke("TryToSpawn", 0.4f);
        }
    }
}
