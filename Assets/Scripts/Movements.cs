﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movements : MonoBehaviour {

    public float movementSpeedX = 3f, movementSpeedY = 3f, movementSpeedZ = 3f, targetPositionX,  returnPositionX, targetPositionY, returnPositionY, targetPositionZ,
        returnPositionZ, rotationSpeedX = 10f, rotationSpeedY = 10f, rotationSpeedZ = 10f;
    public bool horizontalMovementEnabled, verticalMovementEnabled, forwardMovementEnabled, rotationEnabledX, rotationEnabledY ,rotationEnabledZ;

    private Vector3 targetVectorX = Vector3.zero, returnVectorX = Vector3.zero;
    private Vector3 targetVectorY = Vector3.zero, returnVectorY = Vector3.zero;
    private Vector3 targetVectorZ = Vector3.zero, returnVectorZ = Vector3.zero;

    private bool movedHorizontal = false, movedVertical = false, movedForward = false;

    //this script help moving between two coordinates on each axis (simultaneously too) by the added speed; and rotates the object on selected axis by the added speed
    //if you want to use it, simply in the inspector add the script to the gameobject which you want to use this with; set the speed, and the axis positions; set the needed booleans to true

    void Update () {
        SetHorizontal();
        SetVertical();
        SetForward();

        if (horizontalMovementEnabled)
            HorizontalMovement();
        if (verticalMovementEnabled)
            VerticalMovement();
        if (forwardMovementEnabled)
            ForwardMovement();
        if (rotationEnabledX)
            RotationX();
        if (rotationEnabledY)
            RotationY();
        if (rotationEnabledZ)
            RotationZ();
    }


    private void HorizontalMovement()
    {
        if (movedHorizontal == false)
        {
            transform.position = Vector3.MoveTowards(transform.position, targetVectorX, movementSpeedX * Time.deltaTime);
            if (transform.position == targetVectorX)
            {
                movedHorizontal = true;
            }
        }
        else
        {
            transform.position = Vector3.MoveTowards(transform.position, returnVectorX, movementSpeedX * Time.deltaTime);
            if (transform.position == returnVectorX)
            {
                movedHorizontal = false;
            }
        }
    }

    private void VerticalMovement()
    {
        if (movedVertical == false)
        {
            transform.position = Vector3.MoveTowards(transform.position, targetVectorY, movementSpeedY * Time.deltaTime);
            if (transform.position == targetVectorY)
            {
                movedVertical = true;
            }
        }
        else
        {
            transform.position = Vector3.MoveTowards(transform.position, returnVectorY, movementSpeedY * Time.deltaTime);
            if (transform.position == returnVectorY)
            {
                movedVertical = false;
            }
        }
    }

    private void ForwardMovement()
    {
        if (movedForward == false)
        {
            transform.position = Vector3.MoveTowards(transform.position, targetVectorZ, movementSpeedZ * Time.deltaTime);
            if (transform.position == targetVectorZ)
            {
                movedForward = true;
            }
        }
        else
        {
            transform.position = Vector3.MoveTowards(transform.position, returnVectorZ, movementSpeedZ * Time.deltaTime);
            if (transform.position == returnVectorZ)
            {
                movedForward = false;
            }
        }
    }

    private void RotationX()
    {
        transform.Rotate(transform.right, rotationSpeedX * Time.deltaTime);
    }

    private void RotationY()
    {
        transform.Rotate(transform.up, rotationSpeedY * Time.deltaTime);
    }

    private void RotationZ()
    {
        transform.Rotate(transform.forward, rotationSpeedZ * Time.deltaTime);
    }

    private void SetHorizontal()
    {
        targetVectorX.x = targetPositionX;
        targetVectorX.y = transform.position.y;
        targetVectorX.z = transform.position.z;

        returnVectorX.x = returnPositionX;
        returnVectorX.y = transform.position.y;
        returnVectorX.z = transform.position.z;
    }

    private void SetVertical()
    {
        targetVectorY.x = transform.position.x;
        targetVectorY.y = targetPositionY;
        targetVectorY.z = transform.position.z;

        returnVectorY.x = transform.position.x;
        returnVectorY.y = returnPositionY;
        returnVectorY.z = transform.position.z;
    }

    private void SetForward()
    {
        targetVectorZ.x = transform.position.x;
        targetVectorZ.y = transform.position.y;
        targetVectorZ.z = targetPositionZ;

        returnVectorZ.x = transform.position.x;
        returnVectorZ.y = transform.position.y;
        returnVectorZ.z = returnPositionZ;
    }
}
