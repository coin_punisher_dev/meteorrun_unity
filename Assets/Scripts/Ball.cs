﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

public class Ball : MonoBehaviour {

    public ParticleSystem keyUp, splash, splash2;
    public GameObject ringEffect, trayectoryDots;

    [HideInInspector]
    public bool teleporting = false;        //this will be true only while the player is teleporting
    [HideInInspector]
    public bool revived = false;

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Obstacle")       //if we hit an obstacle, then game over
        {
            if(revived)
                EndGame();
        }
        else
        {
            FindObjectOfType<AudioManager>().CollisionSound();      //Plays 'CollisionSound'

            Destroy(Instantiate(keyUp, transform.position, Quaternion.identity).gameObject, 2f);        //spawns a particle to the player's position with the default rotation, then destroys it after 2 secs

            if (collision.relativeVelocity.magnitude > 7.5f)        //if the player of the ball is greater than 7.5
            {
                Destroy(Instantiate(ringEffect, transform.position, Quaternion.identity).gameObject, 1f);       //spawns a particle to the player's position with the default rotation, then destroys it after 1 sec
            }
        }
    }

    public void EndGame()
    {
        if (FindObjectOfType<LevelEnd>().levelDone == false)        //we can not lose if the player gets to the end point
        {
            FindObjectOfType<Trajectory>().enabled = false;

            Time.timeScale = 1f;        //sets back the time to default
            Destroy(Instantiate(splash, transform.position, Quaternion.identity).gameObject, 2f);       //spawns a particle to the player's position with the default rotation, then destroys it after 2 secs
            Destroy(Instantiate(splash2, transform.position, Quaternion.identity).gameObject, 2f);      //spawns a particle to the player's position with the default rotation, then destroys it after 2 secs

            FindObjectOfType<GameManager>().Invoke("EndPanelActivation", 2f);       //calls 'EndPanelActivation' after x secs

            FindObjectOfType<AudioManager>().DeathSound();        //Plays 'DeathSound'

            //Destroy(trayectoryDots);
            //Destroy(gameObject);

            //Makes the player disappear, disables its collider
            GetComponent<Renderer>().enabled = false;
            GetComponent<Rigidbody2D>().simulated = false;
            GetComponent<Collider2D>().enabled = false;
            GetComponent<ParticleSystem>().Stop();
        }
    }

    public void Revive()
    {
        //Revives the player
        revived = false;
        transform.position = new Vector3(0f, transform.position.y + 3f, 0f);
        FindObjectOfType<Trajectory>().enabled = true;
        GetComponent<Renderer>().enabled = true;
        GetComponent<Collider2D>().enabled = true;
        GetComponent<ParticleSystem>().Play();
    }

    public void CanDie()
    {
        revived = true;
    }
}
