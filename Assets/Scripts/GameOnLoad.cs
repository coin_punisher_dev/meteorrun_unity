﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOnLoad : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        PlayerPrefs.GetInt("Audio", 0);
        FindObjectOfType<AudioManager>().soundIsOn = true;
        FindObjectOfType<AudioManager>().PlayBackgroundMusic();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
