﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundSquareSpawner : MonoBehaviour {

    public GameObject[] backgroundSquares;      //if you want to add something to these lists, then add it in the Inspector
    public Transform[] spawnPoints;
    public int maxBgCount = 10;       //maximum of the background squares

    private GameObject randomSquare, actForegroundSquare, actBackgroundSquare;
    private Transform randomSpawnPoint;


    public void Update()
    {
        if (GameObject.FindGameObjectsWithTag("BackgroundSquare").Length <= maxBgCount)        //only spawns background squares if there are less than the max count
        {
            BgSpawn();
        }
    }

    public void FgSpawn()
    {
        randomSquare = backgroundSquares[Random.Range(0, backgroundSquares.Length)];        //selects a random square from the list
        randomSpawnPoint = spawnPoints[Random.Range(0, spawnPoints.Length)];        //selects a random spawn point from the list

        actForegroundSquare = Instantiate(randomSquare, randomSpawnPoint.position, Quaternion.identity);      //spawns the selected square at the selected point whit the default rotation
        actForegroundSquare.GetComponent<Animation>().Play("Rotation" + Random.Range(1, 5).ToString());       //plays one of the 4 rotation animations. if you want to add other animations, you have to increase the number here
      
    }

    public void BgSpawn()
    {
        randomSquare = backgroundSquares[Random.Range(0, backgroundSquares.Length)];        //selects a random square from the list
        randomSpawnPoint = spawnPoints[Random.Range(0, spawnPoints.Length)];        //selects a random spawn point from the list

        actBackgroundSquare = Instantiate(randomSquare, randomSpawnPoint, false);      //spawns the selected square at the selected point whit the default rotation
        actBackgroundSquare.GetComponent<Animation>().Play("Rotation" + Random.Range(1, 5).ToString());     //plays a random rotation animation

        Destroy(actBackgroundSquare, Random.Range(3f, 6.2f));       //destroys it after x seconds
    }
}
