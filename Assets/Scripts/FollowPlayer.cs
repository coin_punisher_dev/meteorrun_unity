﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FollowPlayer : MonoBehaviour {

    public Transform player;
    public Image levelBarImage;
    public float distance = 3f;     //this is the offset. if the player's 'y' position is closer to the camera's 'y' position then we don't move the camera
    public GameObject foreGroundSpawner;

    [HideInInspector]
    public bool levelCleared = false;

    private Vector3 newPosition, player_levelEnd_distance;
    private Transform levelEndTransform;
    private float levelEndDistance, actLevelEndDistance;


	void Start () {
        newPosition = transform.position;
        levelEndTransform = GameObject.FindGameObjectWithTag("LevelEnd").transform;     //finds the level's end position

        player_levelEnd_distance = player.position - levelEndTransform.position;        //calculates the distance between the player's position and the level's end position
        levelEndDistance = player_levelEnd_distance.magnitude;      //magnitude is the distance in float
	}


	void LateUpdate () {
        if (!levelCleared && player != null)     //if the player did not cross the endline yet
        {
            newPosition.y = player.position.y - distance;       //then the camera follows the player on the 'y' axis if it's above the camera by 'distance'

            if (player.position.y - transform.position.y > distance)        //if the camera is moving
            {
                //sets the bar's fillamount relative to the distance between the player and the end of the level
                //we do this here because we don't want the bar to decrease. if we'd like it to do that then we should do this in the 'Ball' script
                actLevelEndDistance = (levelEndTransform.position - player.position).magnitude;
                levelBarImage.fillAmount = 1 - (1f * (actLevelEndDistance / levelEndDistance));

                transform.position = newPosition;

                //spawning background squares
                //if you want more squares, increase the range; if you want less, then decrease it
                if (Random.Range(0, 11) == 5)
                {
                    foreGroundSpawner.GetComponent<BackgroundSquareSpawner>().FgSpawn();
                }
            }
        }
        else if (player != null)
        {
            newPosition.y = player.position.y - distance;       //then the camera follows the player on the 'y' axis if it's above the camera by 'distance'
            transform.position = newPosition;
        }
    }
}
