﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelEnd : MonoBehaviour {

    public bool levelDone = false;      //this will be only true if the player gets to the end of the level

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")       //if the player gets to the end of this level
        {
            FindObjectOfType<FollowPlayer>().levelCleared = true;
            collision.gameObject.GetComponent<Rigidbody2D>().gravityScale = -0.1f;       //the player will be less affected by gravity
            levelDone = true;
            FindObjectOfType<GameManager>().ClearedPanelActivation();       //activates 'ClearedPanel'
            FindObjectOfType<Trajectory>().enabled = false;

            FindObjectOfType<AudioManager>().WinSound();        //Plays 'WinSound'
        }
    }
}
